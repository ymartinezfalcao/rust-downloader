# Simple Rust downloader for testing purposes

As a POC test for seeing the interoperability of Rust and Python in action, I created this repository.
It simply downloads a page after adding it as work.

## How to use
First install cargo to manage your rust application.

Create a virtual environment to install maturin, the rust-python package builder and publisher:

```sh
python3.X -m venv .venv
source .venv/bin/activate
pip install maturin
```

To test it in your virtual environment, simply build it using `maturin develop`

```sh
source .venv/bin/activate
maturin develop
```


Then you can import it as a normal module, as the example `test.py` in the root of this repository.
