use pyo3::prelude::*;
use reqwest;
use std::sync::mpsc::{self, Receiver, Sender};
use std::thread;
use tokio::runtime::Runtime;

#[pyclass]
struct Request {
    url: String,
    headers: Option<String>,
    country: String,
    url_type: String,
    headless: bool,
    proxy: Option<String>,
    json: Option<String>,
    data: Option<String>,
    cookies: Option<String>,
}

#[pymethods]
impl Request {
    #[new]
    fn new(
        url: String,
        headers: Option<String>,
        country: String,
        url_type: String,
        headless: bool,
        proxy: Option<String>,
        json: Option<String>,
        data: Option<String>,
        cookies: Option<String>,
    ) -> Self {
        Request {
            url,
            headers,
            country,
            url_type,
            headless,
            proxy,
            json,
            data,
            cookies,
        }
    }
}

#[pyclass]
struct Downloader {
    runtime: Runtime,
    sender: Sender<Request>,
    receiver: Receiver<Request>,
}

#[pymethods]
impl Downloader {
    #[new]
    fn new() -> Self {
        let runtime = Runtime::new().unwrap();
        let (sender, receiver) = mpsc::channel();
        Downloader {
            runtime,
            sender,
            receiver,
        }
    }

    fn add_work(&self, request: &Request) -> String {
        //let resp = self.runtime.block_on(async {
        //    let url = &request.url;
        //    let r = self.client.get(url).send().await?;
        //    r.text().await
        //});
        let url = Box::new(request.url.to_string());
        thread::spawn(move || {
            let resp = reqwest::blocking::get(url.to_string())
                .expect("It should have a response")
                .text()
                .expect("It should have a text");
            println!("{:#?}", resp);
        });

        "Ok!".to_string()
    }
}

#[pymodule]
fn downloader(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Downloader>()?;
    m.add_class::<Request>()?;
    Ok(())
}
